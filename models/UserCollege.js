"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("UserCollege", {
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		collegeId: {type: DataTypes.INTEGER},
		userId: {type: DataTypes.INTEGER},
		interested: {type: DataTypes.BOOLEAN}
	})
}