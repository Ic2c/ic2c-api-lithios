/**
 * @constant {number} - the default number of items to return per "page"
 */
const DEFAULT_PAGINATION_LIMIT = 20;

/**
 * @constant {number} - the maximum number of items that can be returned per "page"
 */
const PAGINATION_LIMIT_MAX = 100;

module.exports = {
  DEFAULT_PAGINATION_LIMIT,
  PAGINATION_LIMIT_MAX
}