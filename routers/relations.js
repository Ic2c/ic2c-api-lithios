var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var config = require('../dev-config');

var User = models.User;
var Relations = models.Relations;
var Notification = models.Notifications

var jwt = require('jsonwebtoken')

var NotificationService = new (require('../services/NotificationService'));


router.use(function(req, res, next) {
	// check header or url parameters or post parameters for token
	if ('OPTIONS' == req.method) {
		res.sendStatus(204);
	} else {
		var token = req.headers['authorization'];
		// decode token
		if (token) {
			// verifies secret and checks exp
			jwt.verify(token, config.jwtSecret, function(err, decoded) {      
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });    
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;  
					next();
				}
			});
		} else {
			return res.status(403).send({ 
				success: false, 
				message: 'No token provided.' 
			});
		}
	}
});

router.post("/follow", function(req, res) {
	Relations.findAll({
		where: {
			followerId: req.decoded.user_info.id,
			followedId: req.body.id
		}
	}).then(function(relations) {
	  console.log(relations.length)
		if (relations.length == 0 && req.body.id != req.decoded.user_info.id) {
			Relations.create({
				followerId: req.decoded.user_info.id,
				followedId: req.body.id
			}).then(function(response) {
				Notification.create({
					fromUserId: req.decoded.user_info.id,
					toUserId: req.body.id,
					text: "is now following you!",
					seen: false
				})
				User.findOne({
					where: {
						id: req.decoded.user_info.id
					}
				}).then(function(fromuser) {
					User.findOne({
						where: {
							id: req.body.id
						}
					}).then(function(touser) {
						NotificationService.sendNotification(fromuser.name + ' is now following you', touser.pushNotificationId,1)
					})
				})
				res.send({
					success: true
				})
			})
		} else {
			res.send({
				success: true
			})
		}

	})
})

router.post("/unfollow", function(req,res){
	Relations.destroy({where: {
		followerId: req.decoded.user_info.id,
		followedId: req.body.id
	}}).then(function(response){
		res.send({success: true})
	})
})


module.exports = router;
