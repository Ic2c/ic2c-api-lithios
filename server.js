var express = require('express')
var app = express()
var private_router = require('./routers/private_api.js')
var public_router = require('./routers/public_api.js')
var search_router = require("./routers/search.js")
var message_router = require('./routers/message.js')
var relations_router = require('./routers/relations.js')
var models = require('./models')
var config = require('./dev-config')
var bodyParser = require('body-parser')
var morgan = require('morgan')
var server = require('http').createServer(app)
var io = require('socket.io')(server), active_users = {}
var jwt = require('jsonwebtoken')
var socketioJwt = require('socketio-jwt')
require('events').EventEmitter.prototype._maxListeners = 100;

var path = require('path')

//body parsing
app.use(bodyParser.json({ limit: '50mb' }))
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }))

// app.post("/", function(req, res){
//     console.log(req)
// })

app.use(morgan('dev'))
app.set('jwtSecret', config.jwtSecret)

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, X-Response-Time, X-PINGOTHER, X-CSRF-Token,Authorization');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Expose-Headers', 'X-Api-Version, X-Request-Id, X-Response-Time');
  res.header('Access-Control-Max-Age', '1000');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  next();
});

app.use("/private_api", private_router)
app.use("/public_api", public_router)
app.use("/search", search_router)
app.use("/messages", message_router)
app.use("/relations", relations_router)


app.get("/", function (req, res) {
  res.send("Welcome to the iC2C API v3.0!")
})

// app.get("*", function(req, res){
//     res.sendFile(path.join(__dirname + '/IC2C_Website/index.html'));
// })






// io.sockets.on('connection', socketioJwt.authorize({
//     secret: config.jwtSecret,
//     timeout: 15000 // 15 seconds to send the authentication message
//   })).on('authenticated', function(socket) {
//     //this socket is authenticated, we are good to handle more events from it.
//     active_users[socket.decoded_token.user_info.id] = socket

//     socket.on("sendto", function(data, callback){

//         if (data.id in active_users){
//             data['from'] = socket.decoded_token.user_info.id
//             active_users[data.id].emit("getmessage", data)
//             callback(true)
//         }
//         else{
//             callback(false)
//         }
//     })

//     socket.on("disconnect", function(data){
//         delete active_users[socket.decoded_token.user_info.id]
//     })
//   })

models.sequelize.sync().then(function () {

  server.listen(config.port);
  console.log('app is running on port ' + config.port
  );
});

