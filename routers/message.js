var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var config = require('../dev-config');
var Message = models.Message;
var User = models.User;
var jwt = require('jsonwebtoken')
var async = require('async')
var Notification = models.Notifications
var apn = require('apn')



var apnProvider = new apn.Provider({  
	 token: {
			key: 'AuthKey_557T8PBWKE.p8', // Path to the key p8 file
			keyId: '4DK6YGRZHN', // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
			teamId: '3P22SFGPXS', // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
			production: true
	},
	production: true
});


router.use(function(req, res, next) {

	// check header or url parameters or post parameters for token
	if ('OPTIONS' == req.method) {
		res.sendStatus(204);
	} else {
		var token = req.headers['authorization'];
		// decode token
		if (token) {
			// verifies secret and checks exp
			jwt.verify(token, config.jwtSecret, function(err, decoded) {      
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });    
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;  
					next();
				}
			});
		} else {
			return res.status(403).send({ 
				success: false, 
				message: 'No token provided.' 
			});
		}
	}
});

router.post("/send_message", function(req, res){
	Message.create({fromUserId: req.decoded.user_info.id, toUserId: req.body.toUserId, message: req.body.message, seen: false}).then(function(final){
		Notification.create({fromUserId: req.decoded.user_info.id, toUserId: req.body.toUserId, text: "sent you a message", seen: false}).then(function(notif_final){
			User.findOne({where: {id: req.body.toUserId}}).then(function(user){
				var deviceToken = user.pushNotificationId;
				var notification = new apn.Notification();
				notification.topic = 'southpawac.IC2C';
				notification.expiry = Math.floor(Date.now() / 1000) + 3600;
				notification.badge = 1;
				notification.sound = 'default';
				notification.alert = req.body.name+' sent you a message';
				notification.payload = {id: 123};
			 
				apnProvider.send(notification, deviceToken).then(function(response){
					console.log(response.failed[0].response)
				})
				res.send({success: true})
			}).catch(function(error){
				console.log(error)
			})
		 
		})
	})
})
router.get("/messages", function(req, res){
	Message.findAll({
		where: 
		{
			$or: [
				{toUserId: req.decoded.user_info.id}, 
				{fromUserId: req.decoded.user_info.id}
			]
		}
	}).then(function(response){
		//Create an array of messages
		var messages = {}
		if(response.length == 0){
			res.send([])
		}
		else{
		for (message in response){


			var dir = "from"
			var foreign = response[message]['toUserId']
			if(foreign == req.decoded.user_info.id){
				var foreign = response[message]['fromUserId']
				dir = "to"
			}

			if(messages[foreign] == undefined){
				messages[foreign] = {}
			}

			var mess = {text: response[message]['message'], dir: dir, seen: response[message]['seen'], from: response[message]['fromUserId']}


			//Make message seen
			Message.update({seen: true}, {where:{toUserId: req.decoded.user_info.id}})


			if(messages[foreign]['messages'] == undefined){
				messages[foreign]['messages'] = [mess]
			}
			else{
				messages[foreign]['messages'].push(mess)
			}
		}
		var zip_messages = []
		for (var key in messages) {
				var zip = messages[key]
				zip['index'] = key
				zip_messages.push(zip);  
		}

		//fix later for efficiency
		async.map(zip_messages, function(zip_message, cb){
			User.findOne({where: {id: zip_message['index']}}).then(function(user){
				zip_message['name'] = user.dataValues.name
				zip_message['position'] = user.dataValues.sportPosition
				zip_message['image'] = user.dataValues.profileImage
				zip_message['last_message'] = zip_message['messages'][zip_message['messages'].length-1].text

				if(zip_message['messages'][zip_message['messages'].length-1].from != req.decoded.user_info.id){
						zip_message['seen'] = zip_message['messages'][zip_message['messages'].length-1].seen
				}
				else{
						zip_message['seen'] = true
				}
				cb(null, zip_message)
			})
		}, function(err, zm){
		res.send(zm)
		})
		}
	})
})


module.exports = router;
