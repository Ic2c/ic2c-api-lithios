var models = require('../models');
var _model = models.College; 


function CollegeService(){
	function findByName(name){
		return _model.findOne({where: {name: name}}).then(function(college){
			return college
		})
	}
	return {
		findByName: findByName
	}
}

module.exports = CollegeService;