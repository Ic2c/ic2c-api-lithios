"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("Feed",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		userId: {type: DataTypes.INTEGER},
		text: {type: DataTypes.STRING},
		image: {type: DataTypes.STRING},
		video: {type: DataTypes.STRING},
		stamp: {type: DataTypes.DATE},
		imageWidth: {type: DataTypes.INTEGER},
		imageHeight: {type: DataTypes.INTEGER}
	})
}