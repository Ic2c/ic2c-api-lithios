module.exports = coach => {
  const {
    id, email, username, isVerified, name, type, city, state, highschool, coachName, coachEmail, coachPhone, coachPosition, profileImage, sport, division, age, createdAt, updatedAt
  } = coach;
  return {
    id, email, username, isVerified, name, type, city, state, highschool, coachName, coachEmail, coachPhone, coachPosition, profileImage, sport, division, age, createdAt, updatedAt
  }
}


/*
{
          "id": 15,
          "email": "cedwards@adrian.edu",
          "password": null,
          "username": "671952e8-89f2-11e7-8432-3c15c2d8fb18",
          "isVerified": true,
          "name": "Charlie Edwards",
          "sportPosition": null,
          "weight": null,
          "height": null,
          "phone": null,
          "SAT": null,
          "ACT": null,
          "vcode": null,
          "type": "coach",
          "city": null,
          "state": "MI",
          "highschool": "Adrian College",
          "WGPA": null,
          "UWGPA": null,
          "coachName": null,
          "coachEmail": null,
          "coachPhone": null,
          "coachPosition": null,
          "videoLink": null,
          "PSAT": null,
          "profileImage": "https://s3.amazonaws.com/ic2cprofileimages/user1389321491095124587",
          "sport": "Men's Soccer",
          "bio": null,
          "ASVAB": null,
          "bench": null,
          "division": "Division 3",
          "favoriteSubject": null,
          "age": null,
          "resetKey": null,
          "pushNotificationId": null,
          "createdAt": null,
          "updatedAt": null
      }
*/