const express = require('express');
const router = express.Router();
const models = require('../models');
const Sequelize = require('sequelize')
const bcrypt = require('bcrypt')
const saltRounds = 10;

const User = models.User
const config = require('../dev-config')
const jwt = require('jsonwebtoken')
const FeedService = new (require("../services/FeedService"))
const NotificationService = new (require("../services/NotificationService"))

const College = models.College
const uuid = require('uuid/v4');

//Setting up twilio
const accountSid = 'ACc23a334056b0422092b4a209f4ab2def';
const authToken = 'd2357a45edfd455f79a659960e4b0f9f';
const client = require('twilio')(accountSid, authToken);

const constants = require("../constants");


const mailgun = require('mailgun-js')({ apiKey: config.mailgun.api_key, domain: config.mailgun.domain });

// const stripe = require('stripe')('sk_live_FeAUdU24h9BXnOwPF2mf1Aku');
const stripe = require('stripe')('sk_test_OnqJlcReRWz8PeDKDZDnMsgR');

const request = require('request');

const coachSerializer = require("../serializers/coachSerializer");

router.get('/', function (req, res) {
  res.send("Welcome to the public API!")
})

router.post('/sendContactEmail', function (req, res) {

  var data = {
    from: req.body.name + ' <' + req.body.address + '>',
    to: 'quentin@iconnect2colleges.com',
    subject: 'iConnect2Colleges Contact Message',
    text: req.body.message
  };

  console.log(data)

  // send mail with defined transport object
  mailgun.messages().send(data, function (error, body) {
    console.log(error)
    console.log(body)
    if (error) {
      return console.log(error);
    }
    res.send({
      success: true
    })
  });
})
router.post('/createUser', function (req, res) {
  if (req.body.password != null) {
    bcrypt.genSalt(saltRounds, function (err, salt) {
      bcrypt.hash(req.body.password, salt, function (err, hash) {
        if (err) throw err;
        if (req.body.email != null && req.body.username != null && hash != null) {
          User.findAll({
            where: {
              email: req.body.email
            }
          }).then(function (list) {
            if (list.length != 0) {
              res.send({ success: false, path: "email", message: "That email already exists. Please choose another." })
            } else {
              User.create({ username: req.body.username, password: hash, name: req.body.name, email: req.body.email, type: req.body.type }).then(function (response) {
                if (req.body.type != "default") {
                  var token = jwt.sign({ user_info: response }, config.jwtSecret);
                  res.send({ success: true, token: token, user: response })
                } else {
                  var token = jwt.sign({ user_info: response }, config.jwtSecret);
                  res.send({ success: true, token: token, user: response })
                }
                //upload pushNotification on registration
                User.update({ pushNotificationId: req.body.pushNotificationId }, { where: { id: response.id } })
              }).catch(function (err) {
                res.send({ success: false, path: err.errors[0].path, message: "That " + err.errors[0].path + " already exists. Please choose another." })
              });
            }
          })
        } else {
          res.json({ success: false, message: "Username or email not set" })
        }
      });
    });
  } else {
    res.json({ success: false, message: "password not set" })
  }
})


router.post('/resetPassword', function (req, res) {
  bcrypt.genSalt(saltRounds, function (err, salt) {
    console.log(req.body.password)
    bcrypt.hash(req.body.password, salt, function (err, hash) {
      User.update({ password: hash }, {
        where: {
          resetKey: req.body.id
        }
      }).then(function (response) {
        console.log('password reset')
        res.send({ success: true })
      })
    });
  });
})


router.post("/resetPasswordEmail", function (req, res) {
  var resetKey = uuid()
  console.log(req.body.user_email)
  User.findOne({
    where: {
      $or: [{
        username: req.body.user_email
      },
      {
        email: req.body.user_email
      }
      ]
    }
  }).then(function (user) {
    var username = user.dataValues.username
    var email = user.dataValues.email
    User.update({
      resetKey: resetKey
    }, {
        where: {
          username: username
        }
      }).then(function (response) {

        var data = {
          from: '"iConnect2Colleges" <Quentin@iConnect2Colleges.com>',
          to: email,
          subject: 'iConnect2Colleges Forgot Password',
          text: "Here is a link to change your password for iConnect2Colleges: http://www.iconnect2colleges.com/#!/forgotpassword/" + String(resetKey)
        };

        // send mail with defined transport object
        mailgun.messages().send(data, function (error, body) {
          if (error) {
            return console.log(error);
          }
          res.send({
            success: true
          })
        });
      })
  })
})


// ! uncomment to test without auth
router.get('/coaches', async (req, res) => {
  const { limit, offset, ...searchParams } = req.query;
  console.log('limit:', limit)
  console.log('offset:', offset)
  console.log('searchParams:', searchParams)
  // make sure limit and offset are numbers, default limit to constant
  const limitNum = parseInt(limit, 10) || constants.DEFAULT_PAGINATION_LIMIT
  const offsetNum = parseInt(offset, 10);

  // validate query params
  if (offsetNum !== 0 && limitNum !== 0 && (!offsetNum || !limitNum)) {
    res.status(400).json({ success: false, message: 'Pagination required or bad params' })
  }
  if (limitNum > constants.PAGINATION_LIMIT_MAX) {
    res.status(403).json({ success: false, message: `Exceeded maximum pagination limit: ${constants.PAGINATION_LIMIT_MAX}` })
  }

  // if query params include nameLike, convert to Sequelize `$iLike: "str%"` syntax, then remove nameLike from object
  // ? is it necessary to distinguish name and nameLike? Could name always be name LIKE? Perfomance?
  if (searchParams.nameLike) {
    searchParams.name = {
      $like: searchParams.nameLike + "%"
    }
    delete searchParams.nameLike
  }

  // don't let client change "type" here
  if (searchParams.type) delete searchParams.type

  try {
    // get rows and count from User.coach model, providing limit, offset, and searchParams
    const { rows, count } = await User.findAndCountAll(
      {
        where: { type: 'coach', ...searchParams },
        order: [['highschool', 'ASC']],
        limit: limitNum,
        offset: offsetNum
      })

    // define results and pagination metadata
    const results = rows.map(coach => coachSerializer(coach))
    const page = Math.ceil(offsetNum / limit) + 1;
    const pages = Math.ceil(count / limitNum);
    const hasPrevious = offsetNum > 0;
    const previous = hasPrevious ? `&limit=${limit}&offset=${offsetNum - limitNum}` : null;
    const hasNext = count - offsetNum - limitNum > 0;
    const next = hasNext ? `&limit=${limit}&offset=${offsetNum + limitNum}` : null;

    // construct response
    const response = {
      results,
      count,
      limit: limitNum,
      offset: offsetNum,
      page,
      pages,
      hasPrevious,
      previous,
      hasNext,
      next
    }

    // send response
    res.json(response)
  } catch (error) {
    console.log("Error in search: ", error)
    res.status(500).json({ success: false, message: "There was an internal error, please try again." })
  }
})
// !

router.post('/authenticate', function (req, res) {
  User.findOne(
    {
      where: Sequelize.or({
        email: req.body.email
      }, {
          username: req.body.username
        })
    }).then(function (user) {
      if (!user) {
        res.json({ success: false, message: "Authentication failed. User was not found" })
      }
      else {
        console.log(req.body.password)
        bcrypt.compare(req.body.password, user.dataValues.password, function (err, res2) {
          if (res2 == true) {
            var token = jwt.sign({ user_info: user }, config.jwtSecret);

            //update push notification id if it exists
            if (req.body.pushNotificationId != null) {
              User.update({ pushNotificationId: req.body.pushNotificationId }, { where: { username: req.body.username } })
            }
            models.Notifications.update({ seen: true }, { where: { id: user.id } })
            res.json({ success: true, token: token, user: user })
          }
          else {
            res.json({ success: false, message: "Authentication failed. Incorrect username/password combination" })
          }
        });
      }
    })
})



router.get("/get_colleges", function (req, res) {
  User.aggregate('highschool', 'DISTINCT', { plain: false, where: { type: "coach" } })
    .map(function (row) { return row.DISTINCT })
    .then(function (tehValueList) {
      resList = []
      for (li in tehValueList) {
        resList.push({ name: tehValueList[li] })
      }
      res.send(resList)
    })
    ;
})
router.get("/get_divisions", function (req, res) {
  res.send([{ name: "Division 1" }, { name: "Division 2" }, { name: "Division 3" }, { name: "Junior College" }, { name: "NAIA" }])
})
router.get("/get_states", function (req, res) {
  User.aggregate('state', 'DISTINCT', { plain: false, where: { type: "coach" } })
    .map(function (row) { return row.DISTINCT })
    .then(function (tehValueList) {
      resList = []
      for (li in tehValueList) {
        resList.push({ name: tehValueList[li] })
      }

      res.send(resList)
    })
    ;
})
router.get("/get_sports", function (req, res) {
  User.aggregate('sport', 'DISTINCT', { plain: false, where: { type: "coach" } })
    .map(function (row) { return row.DISTINCT })
    .then(function (tehValueList) {
      resList = []
      for (li in tehValueList) {
        resList.push({ name: tehValueList[li] })
      }
      res.send(resList)
    })
    ;
})

router.post("/sendPasswordCode", function (req, res) {
  User.findOne({ where: { $or: [{ email: req.body.email }, { username: req.body.email }] } }).then(function (user) {
    if (user != null) {
      var verification_code = parseInt(Math.random() * 90000) + 10000 - 1
      User.update({ phone: req.body.phone, vcode: verification_code }, { where: { id: user.id } }).then(function (response) {
        client.sms.messages.create({
          to: user.phone,
          from: '+12565889050',
          body: 'Your IC2C forgot password code is: ' + verification_code
        }, function (error, message) {
          if (!error) {
            res.send({ success: true })
          } else {
            res.send({ success: false, message: "That username was not recognized." })
          }
        })
      })
    } else {
      res.send({ success: false, message: "That username was not recognized." })
    }
  })
})

router.post("/updatePassword", function (req, res) {
  User.findOne({ where: { $or: [{ email: req.body.email }, { username: req.body.email }], vcode: req.body.code } }).then(function (user) {
    if (user != null) {
      bcrypt.genSalt(saltRounds, function (err, salt) {
        if (!err) {
          bcrypt.hash(req.body.password, salt, function (err, hash) {
            if (!err) {
              User.update({ password: hash }, { where: { id: user.id } }).then(function (response) {
                res.send({ success: true })
              })
            } else {
              res.send({ success: false })
            }
          })
        } else {
          res.send({ success: false })
        }
      })
    } else {
      res.send({ success: false })
    }
  })
})

router.post("/webhookMessage", function (req, res) {
  if (req.body.app_id == "6E4EA15B-8A10-4744-9254-F3F30BB9DBB9") {
    //There will always be two members for each group.
    var memberId1 = req.body.members[0].user_id
    var memberId2 = req.body.members[1].user_id
    var senderId = req.body.sender.user_id

    models.User.findOne({
      where: {
        id: senderId
      }
    }).then(function (user) {
      var sending = req.body.payload.message
      if (sending == undefined) {
        sending = "sent an image"
      }
      if (memberId1 == senderId) {
        models.User.findOne({
          where: {
            id: memberId2
          }
        }).then(function (toUser) {
          models.Notifications.create({
            fromUserId: memberId1,
            toUserId: memberId2,
            text: "sent you a message.",
            seen: false
          }).then(function (created) {

            models.Notifications.findAll({
              where: {
                toUserId: memberId2,
                seen: false
              }
            }).then(function (notifs) {

              var sending_message = user.name + ' sent you a message.'
              var user_token = toUser.pushNotificationId;
              NotificationService.sendNotification(sending_message, user_token, notifs.length).then(function (notif_res) {
                res.send(notif_res);
              });
            })
          })
        })

      } else {
        models.User.findOne({
          where: {
            id: memberId1
          }
        }).then(function (toUser) {
          models.Notifications.create({
            fromUserId: memberId2,
            toUserId: memberId1,
            text: "sent you a message.",
            seen: false
          }).then(function (created) {
            //post notification!

            models.Notifications.findAll({
              where: {
                toUserId: memberId1,
                seen: false
              }
            }).then(function (notifs) {

              var sending_message = user.name + ' sent you a message.'
              var user_token = toUser.pushNotificationId;
              NotificationService.sendNotification(sending_message, user_token, notifs.length).then(function (notif_res) {
                res.send(notif_res);
              });

            })


          })

        })
      }
    }).catch(function (err) {
      console.log(err)
      res.send({
        success: false
      })
    })
  }
})


router.post("/charge", function (req, res) {
  var token = req.body.token
  var email = req.body.email
  var plan = req.body.plan
  stripe.customers.create({
    email: email,
    source: token
  }, function (err, customer) {
    if (err) {
      console.log(err)
      console.log(customer)
    } else {
      const { id } = customer


      stripe.subscriptions.create({
        customer: id,
        items: [{
          plan: plan
        }]
      }, function (err, subscription) {
        if (err) {
          console.log(err)
          console.log(subscription)
        } else {
          res.send({ success: true })
        }
      })
    }
  })
})








module.exports = router;