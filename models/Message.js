"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("Message",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		toUserId: {type: DataTypes.INTEGER},
		fromUserId: {type: DataTypes.INTEGER},
		message: {type: DataTypes.STRING},
		seen: {type: DataTypes.STRING}
	})
}
