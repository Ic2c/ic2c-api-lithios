var express = require('express');
var router = express.Router();
var models = require('../models');
var Sequelize = require('sequelize');
var config = require('../dev-config');
var User = models.User;
var jwt = require('jsonwebtoken')
var config = require('../dev-config')
var mailgun = require('mailgun-js')({ apiKey: config.mailgun.api_key, domain: config.mailgun.domain });

var SqlString = require('sequelize/lib/sql-string')
//TODO: Put this into a service

const coachSerializer = require("../serializers/coachSerializer");
const constants = require("../constants");

router.use(function (req, res, next) {

  // check header or url parameters or post parameters for token
  if ('OPTIONS' == req.method) {
    res.sendStatus(204);
  }
  else {

    var token = req.headers['authorization'];

    // decode token
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, config.jwtSecret, function (err, decoded) {
        if (err) {
          return res.json({ success: false, message: 'Failed to authenticate token.' });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
        }
      });
    } else {
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });

    }
  }
});



router.get("/", function (req, res) {
  res.send("Welcome to the IC2C search page")
})

router.get('/coaches', async (req, res) => {
  const { limit, offset, ...searchParams } = req.query;
  console.log('limit:', limit)
  console.log('offset:', offset)
  console.log('searchParams:', searchParams)
  // make sure limit and offset are numbers, default limit to constant
  const limitNum = parseInt(limit, 10) || constants.DEFAULT_PAGINATION_LIMIT
  const offsetNum = parseInt(offset, 10);

  // validate query params
  if (offsetNum !== 0 && limitNum !== 0 && (!offsetNum || !limitNum)) {
    res.status(400).json({ success: false, message: 'Pagination required or bad params' })
  }
  if (limitNum > constants.PAGINATION_LIMIT_MAX) {
    res.status(403).json({ success: false, message: `Exceeded maximum pagination limit: ${constants.PAGINATION_LIMIT_MAX}` })
  }

  // if query params include nameLike, convert to Sequelize `$iLike: "str%"` syntax, then remove nameLike from object
  // ? is it necessary to distinguish name and nameLike? Could name always be name LIKE? Perfomance?
  if (searchParams.nameLike) {
    searchParams.name = {
      $like: searchParams.nameLike + "%"
    }
    delete searchParams.nameLike
  }

  // don't let client change "type" here
  if (searchParams.type) delete searchParams.type

  try {
    // get rows and count from User.coach model, providing limit, offset, and searchParams
    const { rows, count } = await User.findAndCountAll(
      {
        where: { type: 'coach', ...searchParams },
        order: [['highschool', 'ASC']],
        limit: limitNum,
        offset: offsetNum
      })

    // define results and pagination metadata
    const results = rows.map(coach => coachSerializer(coach))
    const page = Math.ceil(offsetNum / limit) + 1;
    const pages = Math.ceil(count / limitNum);
    const hasPrevious = offsetNum > 0;
    const previous = hasPrevious ? `&limit=${limit}&offset=${offsetNum - limitNum}` : null;
    const hasNext = count - offsetNum - limitNum > 0;
    const next = hasNext ? `&limit=${limit}&offset=${offsetNum + limitNum}` : null;

    // construct response
    const response = {
      results,
      count,
      limit: limitNum,
      offset: offsetNum,
      page,
      pages,
      hasPrevious,
      previous,
      hasNext,
      next
    }

    // send response
    res.json(response)
  } catch (error) {
    console.log("Error in search: ", error)
    res.status(500).json({ success: false, message: "There was an internal error, please try again." })
  }
})

router.post("/smartsearch", function (req, res) {

  //Create a combined query with all of the words in the query
  var searchText = SqlString.escape(req.body.searchText)
  var searchWords = searchText.split(" ");





  // for (word in searchWords){
  // 	if(searchWords[word] != null && searchWords[word] != ""){
  // 		temp_array = []
  // 		temp_array.push({name: {$like: searchWords[word] + "%"}})
  // 		temp_array.push({sport: {$like: searchWords[word] + "%"}})
  // 		temp_array.push({state: {$like: searchWords[word] + "%"}})
  // 		temp_array.push({division: {$like: searchWords[word] + "%"}})
  // 		temp_array.push({highschool: {$like: searchWords[word] + "%"}})
  // 		query.push({$or: temp_array})
  // 	}
  // }
  // all_query.push({$and: query})


  // // Double word search
  // for (var i = 0; i<(searchWords.length-1); i+=1) {
  // 	all_query.push({name: {$like: searchWords[i]+ " " + searchWords[i+1] + "%"}})
  // 	all_query.push({sport: {$like: searchWords[i]+ " " + searchWords[i+1] + "%"}})
  // 	all_query.push({state: {$like: searchWords[i]+ " " + searchWords[i+1] + "%"}})
  // 	all_query.push({division: {$like: searchWords[i]+ " " + searchWords[i+1] + "%"}})
  // 	all_query.push({highschool: {$like: searchWords[i]+ " " + searchWords[i+1] + "%"}})
  // }


  // Set up real smart search later by combining the words in all possible combinations.
  // for(var num_remove = 0; num_remove<searchWords.length; num_remove+=1){

  // }



  User.findAll({
    attributes: { include: [[Sequelize.literal(`MATCH (name, state, highschool, division, sport) AGAINST(` + searchText + ` IN NATURAL LANGUAGE MODE)`), 'score']] },
    where: Sequelize.literal(`MATCH (name, state, highschool, division, sport) AGAINST(` + searchText + ` IN NATURAL LANGUAGE MODE)`),
    order: [[Sequelize.literal('score'), 'DESC']],
    limit: 45
  }).then(function (users) {
    res.send({ success: true, users: users })
  })


  // User.findAll({where: 
  // 	{
  // 		$or: all_query,
  // 		profileImage: {
  // 			$ne: null
  // 		},
  // 		type: "coach",
  // 	},
  // 	limit: 45
  // })


})

router.post("/name", function (req, res) {
  var searchText = req.body.searchText
  User.findAll(
    {
      where: {
        profileImage: {
          $ne: null
        },
        id: {
          $ne: req.decoded.user_info.id
        },
        name: {
          $like: searchText + "%"
        },
        $or: [
          {
            type: req.body.type1
          }, {
            type: req.body.type2
          }]
      },
      limit: 20,
      offset: req.body.offset

    }).then(function (response) {
      User.count({
        where: {
          id: {
            $ne: req.decoded.user_info.id
          },
          name: {
            $like: searchText + "%"
          },
          $or: [
            {
              type: req.body.type1
            }, {
              type: req.body.type2
            }
          ]
        }
      }).then(function (res2) {
        res.send({ names: response, count: res2 })
      })

    })
})

router.post("/nameUnverified", function (req, res) {
  var searchText = req.body.searchText
  User.findAll(
    {
      where: {
        isVerified: null,
        id: {
          $ne: req.decoded.user_info.id
        },
        name: {
          $like: searchText + "%"
        },
        $or: [
          {
            type: req.body.type1
          }, {
            type: req.body.type2
          }]
      },
      limit: 20,
      offset: req.body.offset

    }).then(function (response) {
      User.count({
        where: {
          id: {
            $ne: req.decoded.user_info.id
          },
          name: {
            $like: searchText + "%"
          },
          $or: [
            {
              type: req.body.type1
            }, {
              type: req.body.type2
            }
          ]
        }
      }).then(function (res2) {
        res.send({ names: response, count: res2 })
      })

    })
})




router.post("/combo", function (req, res) {

  var division = req.body.division
  var sport = req.body.sport
  var state = req.body.state

  User.findAll(
    {
      where: {
        id: {
          $ne: req.decoded.user_info.id
        },
        division: {
          $like: division + "%"
        },
        sport: {
          $like: sport + "%"
        },
        state: {
          $like: state + "%"
        },
        type: 'coach'
      },
      limit: 20,
      offset: req.body.offset

    }).then(function (response) {
      User.count({
        where: {
          id: {
            $ne: req.decoded.user_info.id
          },
          division: {
            $like: division + "%"
          },
          sport: {
            $like: sport + "%"
          },
          state: {
            $like: state + "%"
          },
          type: 'coach'
        },
        limit: 20,
        offset: req.body.offset
      }).then(function (res2) {
        res.send({ names: response, count: res2 })
      })
    })
})


router.post("/school", function (req, res) {
  var searchText = req.body.searchText
  User.findAll(
    {
      where: {
        id: {
          $ne: req.decoded.user_info.id
        },
        highschool: {
          $like: searchText + "%"
        },
        $or: [
          {
            type: req.body.type1
          }, {
            type: req.body.type2
          }
        ]

      },
      limit: 20,
      offset: req.body.offset

    }).then(function (response) {
      User.count({
        where: {
          id: {
            $ne: req.decoded.user_info.id
          },
          highschool: {
            $like: searchText + "%"
          },
          $or: [
            {
              type: req.body.type1
            }, {
              type: req.body.type2
            }
          ]

        }
      }).then(function (res2) {
        res.send({ names: response, count: res2 })
      })
    })
})
router.post("/position", function (req, res) {
  var searchText = req.body.searchText
  User.findAll(
    {
      where: {
        id: {
          $ne: req.decoded.user_info.id
        },
        $and: [{
          $or: [
            {
              sportPosition: {
                $like: searchText + "%"
              }
            },
            {
              sport: {
                $like: req.body.sport + "%"
              }
            }]
        }, {
          $or: [
            {
              type: req.body.type1
            }, {
              type: req.body.type2
            }

          ]
        }
        ]

      },
      limit: 20,
      offset: req.body.offset

    }).then(function (response) {
      User.count({
        where: {
          id: {
            $ne: req.decoded.user_info.id
          },
          $or: [
            {
              sportPosition: {
                $like: searchText + "%"
              }
            },
            {
              sport: {
                $like: req.body.sport + "%"
              }
            }],
          $or: [
            {
              type: req.body.type1
            }, {
              type: req.body.type2
            }
          ]

        }
      }).then(function (res2) {
        res.send({ names: response, count: res2 })
      })
    })
})

router.post("/send_email", function (req, res) {

  var total_from = req.body.name + " <" + req.body.from + ">"
  console.log(total_from)
  var data = {
    from: total_from,
    to: req.body.to,
    subject: "iConnect2Colleges Profile",
    text: req.body.text
  };

  // send mail with defined transport object
  mailgun.messages().send(data, function (error, body) {
    if (error) {
      console.log(error)
      return res.send({ success: false })
    } else {
      console.log(error)
      console.log(body)
      res.send({ success: true })
    }
  });

})
// router.post("/sport", function(req, res){
// 		var searchText = req.body.searchText
// 	User.findAll(
// 		{where: {
// 			id:{
// 				$ne: req.decoded.user_info.id
// 			},
// 			sport: {
// 				$like: searchText + "%"
// 			},
// 			 $or: [
// 			 {
// 			 	type: req.body.type1
// 			 },{
// 			 	type: req.body.type2
// 			 }
// 			 ]
// 		},
// 		limit: 20,
// 		offset: req.body.offset

// 		}).then(function(response){
// 			User.count({where: {
// 			id:{
// 				$ne: req.decoded.user_info.id
// 			},
// 			sport: {
// 				$like: searchText + "%"
// 			},
// 			 $or: [
// 			 {
// 			 	type: req.body.type1
// 			 },{
// 			 	type: req.body.type2
// 			 }]
// 		}}).then(function(res2){
// 			 	res.send({names: response, count: res2})
// 			 })
// 		})
// })


module.exports = router;