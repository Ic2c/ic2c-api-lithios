var express = require('express');
var router = express.Router();
var models = require('../models');
var config = require('../dev-config')
var jwt = require('jsonwebtoken')
var config = require('../dev-config')
var User = models.User

var UserCollege = models.UserCollege
var Notification = models.Notifications
var Relations = models.Relations
var Feed = models.Feed

//Configure twilio
const accountSid = 'ACc23a334056b0422092b4a209f4ab2def';
const authToken = 'd2357a45edfd455f79a659960e4b0f9f';
const client = require('twilio')(accountSid, authToken);


var CollegeService = new(require('../services/College'));
var NotificationService = new(require('../services/NotificationService'));

var async = require('async')
var AWS = require('aws-sdk'),
	fs = require('fs');

var http = require('http');
var FeedService = new(require("../services/FeedService"))
var saltRounds = 10;
var bcrypt = require('bcrypt')

var mailgun = require('mailgun-js')({
	apiKey: config.mailgun.api_key,
	domain: config.domain
});

AWS.config.update({
	accessKeyId: 'AKIAIJXAGWPHCSL2NQIA',
	secretAccessKey: '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'
});

//Actually public but put in private for convenience
router.post("/profile", function(req, res) {
	User.findOne({
		where: {
			username: req.body.username
		},
		include: [{
				model: models.UserCollege,
				include: [{
					model: models.College
				}]
			}
		]
	}).then(function(user) {
		if (req.decoded != null && req.decoded.user_info != null && req.decoded.user_info.id != null && req.decoded.user_info.id != user.dataValues.id) {
			Notification.findOne({
				where: {
					fromUserId: req.decoded.user_info.id,
					toUserId: user.dataValues.id,
					text: "viewed your profile"
				}
			}).then(function(none_response) {
				if (!none_response) {
					Notification.create({
						fromUserId: req.decoded.user_info.id,
						toUserId: user.dataValues.id,
						text: "viewed your profile",
						seen: false
					})
					NotificationService.sendNotification(req.body.name + ' viewed your profile', user.pushNotificationId, 1);
				}
			})
		}
		var user_vals = user.dataValues
		res.send({
			success: true,
			user: user_vals
		})
	})
})


router.use(function(req, res, next) {

	// check header or url parameters or post parameters for token
	if ('OPTIONS' == req.method) {
		res.sendStatus(204);
	} else {
		console.log(req.headers)
		var token = req.headers['authorization'];

		// decode token
		if (token) {
			console.log(token)
			// verifies secret and checks exp
			jwt.verify(token, config.jwtSecret, function(err, decoded) {
				if (err) {
					return res.json({
						success: false,
						message: 'Failed to authenticate token.'
					});
				} else {
					// if everything is good, save to request for use in other routes
					req.decoded = decoded;
					next();
				}
			});
		} else {
			return res.status(403).send({
				success: false,
				message: 'No token provided.'
			});

		}
	}
});

router.post('/connection', function(req, res) {
	User.findOne({
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(user) {
		if (req.body.pushNotificationId != null) {
			User.update({
				pushNotificationId: req.body.pushNotificationId
			}, {
				where: {
					id: req.decoded.user_info.id
				}
			})
			models.Notifications.update({
				seen: true
			}, {
				where: {
					toUserId: req.decoded.user_info.id
				}
			})
		}
		if (user != null && user.age != null && user.age != "") {
			res.send({
				success: true
			})
		} else {
			res.send({
				success: false
			})
		}
	})

})

router.post('/updateFBToken', function(req, res) {
	User.update({
		pushNotificationId: req.body.pushNotificationId
	}, {
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(response) {
		res.send({
			success: true
		})
	})
})

router.post('/connectionv2', function(req, res) {
	User.findOne({
		where: {
			id: req.decoded.user_info.id
		},
		include: [{
				model: Relations,
				as: "following",
				include: [{
					model: User,
					as: "followed_user"
				}]
			},
			{
				model: Relations,
				as: "follower",
				include: [{
					model: User,
					as: "follower_user"
				}]
			},
			{
				model: models.UserCollege,
				include: [{
					model: models.College
				}]
			}
		]
	}).then(function(user) {
		if (req.body.pushNotificationId != null) {
			User.update({
				pushNotificationId: req.body.pushNotificationId
			}, {
				where: {
					id: req.decoded.user_info.id
				}
			})
			models.Notifications.update({
				seen: true
			}, {
				where: {
					toUserId: req.decoded.user_info.id
				}
			})
		}
		if (user != null && user.age != null && user.age != "") {
			res.send({
				success: true,
				user: user
			})
		} else {
			res.send({
				success: false
			})
		}
	})
})


router.post('/verifyPhone', function(req, res) {

	User.findOne({
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(response) {

		User.findOne({
			where: {
				id: {
					$ne: req.decoded.user_info.id
				},
				isVerified: {
					$ne: null
				},
				phone: response.phone
			}
		}).then(function(old_user) {
			if (old_user == null) {
				if (response.vcode == req.body.vcode) {
					User.update({
						isVerified: true
					}, {
						where: {
							id: req.decoded.user_info.id
						}
					}).then(function(worked) {

						res.send({
							success: true,
							user: response
						})
					})
				} else {

					res.send({
						success: false,
						message: "Incorrect verification code"
					})

				}
			} else {
				User.destroy({
					where: {
						id: req.decoded.user_info.id
					}
				}).then(function(rand) {
					//create token for other user and send that token in addition to
					var token = jwt.sign({
						user_info: old_user
					}, config.jwtSecret);

					res.send({
						success: true,
						token: token,
						user: old_user
					})
				})
			}
		})
	}).catch(function(err) {
		console.log("otther err")
		console.log(err)
		res.send({
			success: false,
			message: err
		})
	})
})

//for sending messages

// for (number in numbers){
//   client.messages.create({
//         to: numbers[number],
//         from:'+19193361029',
//         body:'Please update to our latest ic2c version so you can take advantage of all the new features. Also, notice we have changed AGE to GRADE.'
//       }, function(error, message) {

//         if (!error) {
//           console.log({success: true})
//         } else {
//           console.log({success: false, message: "Invalid Phone Number. Please enter another."})
//         }
//   })
// }


router.post('/sendInvites', function(req, res) {
	for (number in req.body.numbers) {
		var the_number = req.body.numbers[number]

		client.messages.create({
			to: the_number,
			from: '+12565889050',
			body: 'What\'s up. I just made my iC2C profile. Download this app and get connected to college coaches! https://itunes.apple.com/us/app/ic2c/id1239212280'
		}, function(error, message) {

			if (!error) {
				console.log({
					success: true
				})
			} else {
				console.log({
					success: false,
					message: "Invalid Phone Number. Please enter another."
				})
			}
		})
	}
})

router.post('/phone', function(req, res) {
	console.log(res.body)
	var verification_code = parseInt(Math.random() * 90000) + 10000 - 1
	User.findAll({
		where: {
			phone: req.body.phone,
			age: {
				$ne: null
			}
		}
	}).then(function(users) {
		if (users.length == 0) {
			User.update({
				phone: req.body.phone,
				vcode: verification_code
			}, {
				where: {
					id: req.decoded.user_info.id
				}
			}).then(function(response) {
				client.messages.create({
					to: req.body.phone,
					from: '+12565889050',
					body: 'Your IC2C verification code is: ' + verification_code
				}, function(error, message) {
					if (!error) {
						res.send({
							success: true
						})
					} else {
						console.log(error)
						res.send({
							success: false,
							message: "Invalid Phone Number. Please enter another."
						})
					}
				})
			})
		} else {
			res.send({
				success: false,
				message: "This phone number is already in use."
			})
		}
	})

})
router.post('/coach1', function(req, res) {

	User.update({
		city: req.body.city,
		state: req.body.state,
		coachPosition: req.body.coachPosition,
		bio: req.body.bio,
		highschool: req.body.school
	}, {
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(response) {
		res.send({
			success: true
		})
	}).catch(function(err) {
		res.send({
			success: false,
			message: err
		})
	})
})
router.post('/player1', function(req, res) {
	User.update({
		city: req.body.city,
		state: req.body.state,
		highschool: req.body.highschool,
		sport: req.body.sport,
		coachName: req.body.coachName,
		coachEmail: req.body.coachEmail,
		coachPhone: req.body.coachPhone,
		bio: req.body.bio
	}, {
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(response) {
		res.send({
			success: true
		})
	}).catch(function(err) {
		res.send({
			success: false,
			message: err
		})
	})
})
router.post('/player2', function(req, res) {
	var b = req.body
	User.update({
		sportPosition: b.position,
		height: b.height,
		weight: b.weight,
		ACT: b.ACT,
		SAT: b.SAT,
		WGPA: b.WGPA,
		UWGPA: b.UWGPA,
		ASVAB: b.ASVAB,
		age: b.age,
		course: b.favoriteSubject
	}, {
		where: {
			id: req.decoded.user_info.id
		}
	}).then(
		function(response) {
			res.send({
				success: true
			})
		}).catch(function(err) {
		res.send({
			success: false,
			message: err
		})
	})
})

router.post('/iminterested', function(req, res) {
	var schools = req.body.schools.schools


	async.map(schools, function(name, cb) {

		CollegeService.findByName(name).then(function(id) {
			if (id != null) {
				cb(null, id.dataValues.id)
			} else {
				cb()
			}
		})

	}, function(err, ids) {
		if (err) throw err;
		var records = []
		for (id in ids) {
			records.push({
				userId: req.decoded.user_info.id,
				collegeId: ids[id],
				interested: true
			});
		}

		UserCollege.destroy({
			where: {
				userId: req.decoded.user_info.id,
				interested: true
			}
		}).then(function(destroyed) {
			UserCollege.bulkCreate(records).then(function(response) {
				res.send({
					success: true
				})
			})
		})
	})
})
router.post('/interestedinme', function(req, res) {
	var schools = req.body.schools.schools
	async.map(schools, function(name, cb) {
		CollegeService.findByName(name).then(function(id) {
			if (id != null) {
				cb(null, id.dataValues.id)
			} else {
				cb()
			}
		})
	}, function(err, ids) {
		if (err) throw err;
		var records = []
		for (id in ids) {
			records.push({
				userId: req.decoded.user_info.id,
				collegeId: ids[id],
				interested: false
			});
		}
		UserCollege.destroy({
			where: {
				userId: req.decoded.user_info.id,
				interested: false
			}
		}).then(function(destroyed) {
			UserCollege.bulkCreate(records).then(function(response) {
				res.send({
					success: true
				})
			})
		})

	})

})

//TODO
router.post('/updateCoach', function(req, res) {

})


router.post("/saveEdit", function(req, res) {
	params = {}
	params[req.body.param] = req.body.value
	User.update(params , {
		 where: {
			id: req.decoded.user_info.id
	   }
	}).then(function(response){

	})
})

router.get("/self_profile", function(req, res) {
	User.findOne({
		where: {
			id: req.decoded.user_info.id
		},
		include: [{
				model: models.UserCollege,
				include: [{
					model: models.College
				}]
			},
			{
				model: Relations,
				as: "following",
				include: [{
					model: User,
					as: "followed_user"
				}]
			},
			{
				model: Relations,
				as: "follower",
				include: [{
					model: User,
					as: "follower_user"
				}]
			}
		]
	}).then(function(user) {
		if (req.decoded.user_info.id != user.dataValues.id) {
			Notification.create({
				fromUserId: req.decoded.user_info.id,
				toUserId: user.dataValues.id,
				text: "viewed your profile",
				seen: false
			})
		}
		var user_vals = user.dataValues
		res.send({
			success: true,
			user: user_vals
		})
	})
})


router.post("/update_password", function(req, res) {
	bcrypt.genSalt(saltRounds, function(err, salt) {
		console.log(req.body.password)
		bcrypt.hash(req.body.password, salt, function(err, hash) {
			User.update({
				password: hash
			}, {
				where: {
					id: req.decoded.user_info.id
				}
			}).then(function(response) {
				res.send({
					success: true
				})
			})
		});
	});
})

router.post('/update_user', function(req, res) {
	console.log(req.decoded.user_info.id);
	User.findOne({
		where: {
			id: req.decoded.user_info.id
		}
	}).then(function(old_user) {
		var old_video = old_user.videoLink
		User.update(req.body.user, {
			where: {
				id: req.decoded.user_info.id
			}
		}).then(function(nothing) {
			User.findOne({
				where: {
					id: req.decoded.user_info.id
				}
			}).then(function(new_user) {
				if (new_user.videoLink != old_video && new_user.videoLink != "") {
					FeedService.addFeed(new_user.id, "added a profile video", "", new_user.videoLink)
				}
				res.send(new_user)
			})
		}).catch(function(err) {
			console.log(err)
			res.send({
				success: false
			})
		});
	}).catch(function(err) {
		console.log(err)
		res.send({
			success: false
		})
	})
})
router.post('/userById', function(req, res) {
	User.findOne({
		where: {
			id: req.body.id
		}
	}).then(function(response) {
		res.send(response.dataValues)
	})
})

router.post("/upload_image", function(req, res) {
	//get image size
	var stamp = new Date().getTime();
	var s3 = new AWS.S3();
	s3.putObject({
		Bucket: 'ic2cprofileimages',
		Key: 'user' + req.decoded.user_info.id + stamp,
		Body: new Buffer(req.body.imageData.split(',')[1], 'base64'),
		ContentEncoding: 'base64',
		ContentType: 'image/jpeg'
	}, function(err, data) {
		User.update({
			profileImage: 'https://s3.amazonaws.com/ic2cprofileimages/user' + req.decoded.user_info.id + stamp
		}, {
			where: {
				id: req.decoded.user_info.id
			}
		}).then(function(result) {
			Feed.destroy({
				where: {
					userId: req.decoded.user_info.id,
					imageWidth: {
						$ne: null
					},
					imageHeight: {
						$ne: null
					}
				}
			}).then(function(feeds) {
				FeedService.addFeed(req.decoded.user_info.id, "added a new profile image", 'https://s3.amazonaws.com/ic2cprofileimages/user' + req.decoded.user_info.id + stamp, "", req.body.imageWidth, req.body.imageHeight)
			})
			res.send({
				success: true,
				profileImage: 'https://s3.amazonaws.com/ic2cprofileimages/user' + req.decoded.user_info.id + stamp
			})
		})
	})
})

router.get('/', function(req, res) {
	res.send("Welcome to the private API!")
})

router.post('/feed', function(req, res) {
	FeedService.getFeed(10, req.body.offset).then(function(feed) {
		res.send({
			feed: feed,
			offset: req.body.offset
		})
	})
})


router.post('/emailInvite', function(req, res) {
	var to_user = req.address

	// setup email data with unicode symbols

	var data = {
		from: '"' + req.decoded.user_info.name + '" <' + req.decoded.user_info.email + '>',
		to: req.body.address,
		subject: 'Invited to iConnect2Colleges',
		text: 'You have been invited to iConnect2Colleges by ' + req.decoded.user_info.name + ". Sign up at www.iconnect2colleges.com"
	};

	mailgun.messages().send(data, function(error, body) {
		if (error) {
			return console.log(error);
		}
		res.send({
			success: true
		})
	});


})


router.get('/notifications', function(req, res) {
	Notification.findAll({
		where: {
			toUserId: req.decoded.user_info.id
		},
		include: [{
			model: User,
			as: "user"
		}],
		limit: 10,
		order: [['createdAt', 'DESC']]
	}).then(function(response) {
		Notification.update({
			seen: true
		}, {
			where: {
				toUserId: req.decoded.user_info.id
			}
		})
		res.send(response)
	})
})

router.post('/dethrone', function(req, res) {
	User.destroy({
		where: {
			username: req.body.username,
			isVerified: null,
			type: "coach"
		}
	}).then(function(response) {
		res.send({
			success: true
		})
	})
})

router.post('/phoneUsers', function(req, res) {
	//make or list
	var orlist = []

	for (phone in req.body.phones) {
		orlist.push({
			phone: req.body.phones[phone]
		})
	}
	//make request
	User.findAll({
		where: {
			$or: orlist,
			age: {
				$ne: null
			}
		}
	}).then(function(users) {

		res.send({
			success: true,
			users: users
		})
	})
})


module.exports = router;