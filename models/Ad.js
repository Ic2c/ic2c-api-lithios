"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("Ad",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		logo: {type: DataTypes.STRING},
		text: {type: DataTypes.STRING},
		image: {type: DataTypes.STRING},
		imageWidth: {type: DataTypes.INTEGER},
		imageHeight: {type: DataTypes.INTEGER},
		link: {type: DataTypes.STRING}
	})
}