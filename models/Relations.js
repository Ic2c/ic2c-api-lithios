"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("Relations",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		followerId: {type: DataTypes.INTEGER},
		followedId: {type: DataTypes.INTEGER}
	})
}