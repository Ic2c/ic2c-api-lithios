"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("Notifications",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		toUserId: {type: DataTypes.INTEGER},
		fromUserId: {type: DataTypes.INTEGER},
		text: {type: DataTypes.STRING},
		seen: {type: DataTypes.BOOLEAN}
	})
}