"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("College",{
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		name: {type: DataTypes.STRING}
	})
}
