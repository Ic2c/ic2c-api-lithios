var models = require('../models');
var sequelize = require("sequelize")

var async = require('async')
var _model = models.Feed; 
var User = models.User;



var request = require('request');
var rp = require('request-promise');

var apn = require('apn')
var apnProvider = new apn.Provider({  
     token: {
        key: 'AuthKey_557T8PBWKE.p8', // Path to the key p8 file
        keyId: '4DK6YGRZHN', // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
        teamId: '3P22SFGPXS', // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
        production: true
    },
    production: true
});




function NotificationService(){

  function sendMessageToUser(deviceId, message) {
    return rp({
      url: 'https://fcm.googleapis.com/fcm/send',
      method: 'POST',
      headers: {
        'Content-Type' :' application/json',
        'Authorization': 'key=AIzaSyD81vj5miJmdk2aaQlbomKljbEtKJXgSvc'
      },
      body: JSON.stringify(
        { 
        	"data" : {
      			"message" : message,
      			"title" : "iC2C"
    		},
          	"to" : deviceId,
          	"priority" : "high"
        }
      )
    }).then(function(response){
    	console.log(response)
    	return {success: true}
    })
  }

	function sendNotification(message, token, badge_number){
		if(token.length <= 64){
            //IOS
            var notification = new apn.Notification();
            notification.topic = 'southpawac.IC2C';
            notification.expiry = Math.floor(Date.now() / 1000) + 3600;
            notification.badge = badge_number;
            notification.sound = 'default';
            notification.alert = message;
            notification.payload = {id: 123};
            console.log(token)
            return apnProvider.send(notification, token).then(function(response){
              	return {success: true}
            })
        }else{
        	console.log("sending android")
        	console.log(token)
          	//ANDROID
          	return sendMessageToUser(token,message);
 
        }
	}

	return {
		sendNotification: sendNotification
	}
}

module.exports = NotificationService;