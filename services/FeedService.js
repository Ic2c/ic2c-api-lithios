var models = require('../models');
var _model = models.Feed; 
var sequelize = require("sequelize")
var User = models.User;
var async = require('async')


function FeedService(){
	function getFeed(amount, offset){
		return _model.findAll({
			order: 'id DESC',
			limit: 15,
			offset: 15*offset,
			include: [
				{model: User, as: "user"}
			]
		}).then(function(feed_list){
			//add an advertisment to the list randomly
			return models.Ad.findAll( {order: [sequelize.fn( 'RAND' )], limit: 3}).then(function(ad){
				feed_list.splice(4, 0, ad[0]);
				feed_list.splice(9, 0, ad[1]);
				feed_list.splice(14, 0, ad[2]);
				return feed_list
			})
		})
	}
	function addFeed(userId, text, image, video, width, height){
		return _model.create({userId: userId, text: text, image: image, video: video, stamp: sequelize.fn('NOW'), imageWidth: width, imageHeight: height}).then(function(feed_list){
			return feed_list
		})
	}
	return {
		addFeed: addFeed,
		getFeed: getFeed
	}
}

module.exports = FeedService;