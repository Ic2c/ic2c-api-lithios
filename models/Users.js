"use strict";

module.exports = function(sequelize, DataTypes){
	return sequelize.define("User", {
		id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, unique: true},
		email: {type: DataTypes.STRING},
		password: {type: DataTypes.STRING},
		username: {type: DataTypes.STRING, unique: true},
		isVerified: {type: DataTypes.BOOLEAN},
		name: {type: DataTypes.STRING},
		sportPosition: {type: DataTypes.STRING},
		weight: {type: DataTypes.STRING},
		height: {type: DataTypes.STRING},
		phone: {type: DataTypes.STRING},
		SAT: {type: DataTypes.STRING},
		ACT: {type: DataTypes.STRING},
		vcode: {type: DataTypes.INTEGER},
		type: {type: DataTypes.STRING},
		city: {type: DataTypes.STRING},
		state: {type: DataTypes.STRING},
		highschool: {type: DataTypes.STRING},
		WGPA: {type: DataTypes.STRING},
		UWGPA: {type: DataTypes.STRING},
		coachName: {type: DataTypes.STRING},
		coachEmail: {type: DataTypes.STRING},
		coachPhone: {type: DataTypes.STRING},
		coachPosition: {type: DataTypes.STRING}, 
		videoLink: {type: DataTypes.STRING}, 
		PSAT: {type: DataTypes.STRING},
		profileImage: {type: DataTypes.STRING},
		sport: {type: DataTypes.STRING},
		bio: {type: DataTypes.STRING},
		ASVAB: {type: DataTypes.STRING},
		bench: {type: DataTypes.STRING},
		division: {type: DataTypes.STRING},
		favoriteSubject: {type: DataTypes.STRING},
		age: {type: DataTypes.STRING},
		resetKey: {type: DataTypes.STRING},
		pushNotificationId: {type: DataTypes.STRING}
	}, {
    indexes: [
      { type: 'FULLTEXT', name: 'name_index', fields: ['name', 'state', 'highschool', 'division', 'sport'] },
  
    ]
  })
}